from our_celery_app import app
import tasks
import os

os.environ["FORKED_BY_MULTIPROCESSING"] = "1"
argv = ["worker", "--loglevel", "info", "--concurrency", "5"]
app.worker_main(argv)
