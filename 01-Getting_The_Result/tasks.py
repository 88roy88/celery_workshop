from our_celery_app import app
import requests
import time

# Example from slides
@app.task
def add(x, y):
    print("add {} + {}".format(x, y))
    time.sleep(5)
    return x + y


"""
Server url:
    http://localhost:8484

List of APIs:
    GET     /users           - show list of users in system 
    POST    /new/{name}      - creates a new user with a specific name (this is a long task)
    GET     /user/{name}/id  - gets the user's system UUID
    GET     /reset           - reset the system database
"""


# TODO: copy your task from the prev ex to here


# TODO: create a new task that returns the user UUID



"""
Hint: to make a request an endpoint you can use the requests library:
    response = requests.get('https://example.com/endpoint')
    print(response.json())

Hint: to make a request and send Post data to an endpoint you can use the requests library:
    response = requests.post('https://example.com/endpoint', data={'key': 'value'})
    print(response.json())
"""
