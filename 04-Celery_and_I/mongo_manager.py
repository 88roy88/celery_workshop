from typing import List, Dict, Any

import pymongo


class MongoManager:
    def __init__(self, mongo_url: str, collection_name: str) -> None:
        self._client = pymongo.MongoClient(mongo_url)
        self.collection_name = collection_name
        self.collection = None
        self.init_connect()

    def init_connect(self) -> None:
        self.collection = self._client["db"][self.collection_name]

    @property
    def count(self) -> int:
        return self.collection.count_documents({})

    def get_count(self, key: str, value: str) -> int:
        return self.collection.count_documents({key: value})

    def get_count_of_key(self, key: str) -> int:
        return self.collection.count_documents({key: {"$exists": True}})

    def get_by(self, key: str, value: str) -> List[Dict[str, Any]]:
        return list(self.collection.find({key: value}))

    def get_unique_values(self, key: str) -> List[str]:
        return self.collection.distinct(key)

    def get_all_values(self) -> List[Dict[str, Any]]:
        return list(self.collection.find({}))

    def drop_collection(self) -> None:
        self.collection.drop()
        self.init_connect()

    def upsert(self, data: Dict[str, Any]) -> None:
        self.collection.update_one({"_id": data["_id"]}, {"$set": data}, upsert=True)

mongoClient = MongoManager("mongodb://localhost:27018/", "db")


if __name__ == "__main__":
    print(mongoClient.get_all_values())
    mongoClient.upsert({"_id": "1", "name": "test"})
    print(mongoClient.get_all_values())
    mongoClient.upsert({"_id": "1", "age": "33"})
    mongoClient.upsert({"_id": "2", "name": "test2"})
    print(mongoClient.get_all_values())
    mongoClient.drop_collection()