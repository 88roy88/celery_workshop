from our_celery_app import app

"""
Server url:
    http://localhost:8484

List of APIs:
    POST     /UniSecure/parse_line     - parsing a line (getting the line as the data/body for the post request), 
                                            returns 
    POST     /UniSecure/country        - take ip as the body of the request, returns json of {"country": <country_name>}
    POST     /UniSecure/browser        - take user_agent as the body of the request, returns json of {"browser": <browser_name>}
    POST     /UniSecure/os             - take user_agent as the body of the request, returns json of {"os": <os_name>}

Reminder: to make a request and send Post data to an endpoint you can use the requests library:
import requests
response = requests.post('https://example.com/endpoint', data="my_data!!!")
print(response.json())

"""


# TODO: you go champs!