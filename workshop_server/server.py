import uuid
import uvicorn
import time
import random

from fastapi import FastAPI, HTTPException, Request

app = FastAPI()

db = {}


@app.get("/new/{name}")
@app.post("/new/{name}")
def new_user(name: str):
    time.sleep(5)
    db[name] = {"name": name, "id": f"USER-{uuid.uuid4()}-USER"}
    return "hello " + name


@app.get("/user/{name}/id")
def new_user(name: str):
    time.sleep(2)
    return {name: db[name]["id"]}


@app.get("/user/{id}/age/{age}")
@app.post("/user/{id}/age/{age}")
@app.put("/user/{id}/age/{age}")
def set_age(id: str, age: int):
    time.sleep(2)
    for user in db:
        if db[user]["id"] == id:
            db[user]["age"] = age
            return {"id": id, "name": db[user]["name"], "age": age}


@app.get("/user/{name}/add_num/{num}")
@app.post("/user/{name}/add_num/{num}")
@app.put("/user/{name}/add_num/{num}")
def add_num(name: str, num: int):
    time.sleep(random.randint(10, 35) / 10)
    curr_list = db[name].get("num_list", [])
    curr_list.append(num)
    db[name]["num_list"] = curr_list
    return db[name]


@app.get("/unstable_endpoint")
def unstable_endpoint():
    time.sleep(0.5)
    if random.randint(0, 6) != 6:
        raise HTTPException(status_code=418, detail="may the odds be ever in your favor!")
    return {"yes!": "we did it!"}


@app.get("/user/{name}/num_sum")
def get_sum(name: str):
    time.sleep(1)
    num_list = db[name].get("num_list", [])
    return sum(num_list)


@app.get("/users")
def get_users():
    return {"users": list(db.keys())}


@app.get("/reset")
def reset():
    db.clear()
    return {"message": "db reset"}


@app.get("/")
def main():
    return {"users": db}

##### Celery & I

from apachelogs import LogParser
import geoip2.database
from user_agents import parse

PARSER = LogParser("%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"")
reader = geoip2.database.Reader('GeoLite2-Country.mmdb')
UNKNOWN = "UNKNOWN"


@app.post("/UniSecure/pasre_line")
async def parse_line(request: Request):
    req_body = await request.body()
    line_to_parse = req_body.decode("utf-8")
    parsed_line = PARSER.parse(line_to_parse)
    parsed_line_as_dict = parsed_line.__dict__
    extra_headers = parsed_line.headers_in
    for key in extra_headers:
        key_snake_case = key.replace("-", "_").lower()
        parsed_line_as_dict[key_snake_case] = extra_headers[key]
    return parsed_line_as_dict




@app.post("/UniSecure/country")
async def get_ip(request: Request):
    req_body = await request.body()
    ip = req_body.decode()
    country_name = UNKNOWN
    try:
        country = reader.country(ip)
        country_name = country.country.name or UNKNOWN
    except geoip2.errors.AddressNotFoundError:
        pass
    return {"country": country_name}

@app.post("/UniSecure/browser")
async def get_browser(request: Request):
    req_body = await request.body()
    user_agent = req_body.decode()
    browser = UNKNOWN
    if user_agent:
        user_agent_parsed = parse(user_agent)
        browser = user_agent_parsed.browser.family
    return {"browser": browser}

@app.post("/UniSecure/os")
async def get_os(request: Request):
    req_body = await request.body()
    user_agent = req_body.decode()
    os = UNKNOWN
    if user_agent:
        user_agent_parsed = parse(user_agent)
        os = user_agent_parsed.os.family
    return {"os": os}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=80)

