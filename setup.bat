@echo off

REM Check if docker is installed
where docker >nul 2>nul
if %errorlevel% neq 0 (
  echo docker is not installed
  echo Please install Docker if you want to run the workshop locally
  exit /b 1
)

REM Check if the Docker Desktop executable file exists
if not exist "C:\Program Files\Docker\Docker\Docker Desktop.exe" (
  echo Docker Desktop is not found in the specified location.
  echo Please verify the installation path or just open it manually.
)

echo Start Docker Desktop
start "" /b "C:\Program Files\Docker\Docker\Docker Desktop.exe"

:LOOP
docker ps | findstr "CONTAINER ID" >nul 2>nul
if %errorlevel% neq 0 (
  echo Waiting for Docker
  timeout /t 5 >nul
  goto LOOP
)

echo Docker is up

echo Run Redis
docker run --rm -p 6380:6379 --name celery-workshop-redis -d redis


echo Run Mongo
docker run --rm -p 27018:27017 --name celery-workshop-mongo -d mongo


:: echo Run Postgres
:: docker run --rm --name postgres-for-celery-workshop -e POSTGRES_PASSWORD=postgres -e POSTGRES_USERNAME=postgres -e POSTGRES_DB=test_db -p 5430:5432 -d postgres

echo Running mock server
docker build -t celery_workshop_server -f .\workshop_server\Dockerfile .\workshop_server
docker run --rm -p 8484:80 --name celery_workshop_server -d celery_workshop_server
