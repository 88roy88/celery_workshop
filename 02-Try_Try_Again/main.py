
from tasks import my_task
# from tasks import <YOUR_TASK>
import time

def old_main():
    task = my_task.delay([1,2,3])
    while not task.ready():
        print(task.status, task.state, task.result)
        time.sleep(0)
    print(task.status, task.state, task.result)
    print(task.get())

def main():
    # TODO: call your task and print the secret msg
    pass

if __name__ == "__main__":
    main()