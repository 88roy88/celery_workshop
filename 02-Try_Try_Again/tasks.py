from our_celery_app import app
import time
import requests
import random

class FailedException(Exception):
    pass

# random code with 30% success rate
def fail_sometime():
    random_number = random.randint(0, 9)
    time.sleep(0.2)
    if random_number > 2:
        raise FailedException("random exception")


# Example from slides
@app.task(bind=True)
def my_task(self, other_params):
    print(f"wow! what cool params: {other_params}")
    self.update_state(state="IN-PROGRESS", meta={"progress": 0})

    try:
        fail_sometime()
    except FailedException as e:
        num_of_retries = self.request.retries
        print(f"failed for the {num_of_retries+1} time")
        raise self.retry(exc=e, countdown=1, max_retries=10)
    
    return "OK :)"



"""
Server url:
    http://localhost:8484

List of APIs:
    GET     /unstable_endpoint  - endpoint with secret msg, but it fails most of the time ;)
"""

# TODO: add here your task here, having up to 20 retries with 1 second delay 
# also update the task status on each retry 


"""
Hint:
    you can access the num of retires the task had so far by using
    self.request.retries

Hint: 
    after making a request using the requests library, 
    you can call raise_for_status to raise an exception if the request failed.
    
    for example:
    response = requests.get('https://example.com/will_fail')
    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        print(f"request failed with status code {status_code}")

"""

