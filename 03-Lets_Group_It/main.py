# from tasks import <YOUR_TASK>
from celery import chain, group


def old_main():
    from tasks import add, mul
    import time

    c = chain(add.s(5, 5), add.s(10), mul.si(5, 20), mul.s(10))
    task_c = c.delay()
    task_res = task_c.get()
    print(task_res)

    g = group(add.s(5, 5), add.s(10, 10), mul.si(5, 20), mul.s(10, 10))
    task_g = g()
    print(task_g.completed_count())
    time.sleep(3)
    print(task_g.completed_count())

    group_res = task_g.get()
    print(group_res)
    print(task_g.completed_count())


def main():
    # TODO: add your code here
    # create a chain that: create_user -> get_the_user_id -> set_the_user_age
    
    # create a group that add few numbers for your users
    pass


if __name__ == "__main__":
    main()
