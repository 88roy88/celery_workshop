from our_celery_app import app
import requests
import time


# Example Task
@app.task
def mul(x, y):
    time.sleep(2)
    return x * y

# Example Task
@app.task
def add(x, y):
    time.sleep(2)
    return x + y



"""
Server url:
    http://localhost:8484

List of APIs:
    GET     /users                      - show list of users in system 
    POST    /new/{name}                 - creates a new user with a specific name (this is a long task)
    GET     /user/{name}/id             - gets the user's system UUID
    POST    /user/{id}/age/{age}        - sets the user age
    PUT     /user/{name}/add_num/{num}  - add a number to a list of numbers that the user likes or something idk... XD
    GET     /reset                      - reset the system database
"""


# TODO: copy yours tasks from prev ex. 

# TODO: create new task for setting the user age

# TODO: create new task for adding number to the user list
