# Celery Workshop by Roy M Mezan

## Requirements
- Python (tested on 3.9)
- Docker
- This repo :)

## Installation
1. run the setup.sh/setup.bat script, it will run a redis-server, a mongodb-server, and a mock server to play around with
2. install python requirements.txt file `pip install -r requirements.txt`, Recommended on a venv

## Running an exercise
0. ```cd 0X-EXERCISE-NAME```
1. add your code at both `main.py`, `task.py`
2. run the worker by running ```python worker_starter.py``` at the specific ex folder
   * reminder you'll need to stop the worker (`ctrl-c`) and re-run it on any change at `task.py`
3. run `main.py` -->  ```python main.py```



---
### © 2021-2023 Roy M Mezan
- Ownbackup internal workshops
- EuroPython-2023