# check if docker is installed
if ! [ -x "$(command -v docker)" ]; then
  echo "docker isn't installed"
  echo "please install docker if you want to run the workshop locally"
  exit 1
fi

echo "open docker"
open --background -a Docker

while (!(docker ps | grep "CONTAINER ID"))
do
  echo "waiting for docker"
  sleep 5
done
echo "docker is up"

echo "run redis"
docker run --rm -p 6380:6379 --name celery-workshop-redis -d redis

echo "run mongo"
docker run --rm -p 27018:27017 --name celery-workshop-mongo -d mongo

# echo "run postgres"
# docker run --rm --name postgres-for-celery-workshop -e POSTGRES_PASSWORD=postgres -e POSTGRES_USERNAME=postgres -e POSTGRES_DB=test_db -p 5430:5432 -d postgres

echo "running mock server"
docker build -t celery_workshop_server -f ./workshop_server/Dockerfile ./workshop_server
docker run --rm -p 8484:80 --name celery_workshop_server -d celery_workshop_server 